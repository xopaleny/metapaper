import './App.css';
import PaperNameGenerator from "./components/PaperNameGenerator";
import visitlabLogo from "./images/visitlab_logo.png";

function App() {
  return <>
    <h2>
      <a href="https://visitlab.fi.muni.cz/"><img alt="Visitlab logo" src={visitlabLogo} className="logo-text" /></a>
    </h2>
    <div className="content">
      <span className="blurb">Have some inspiration for your next visualization paper!</span>
      <PaperNameGenerator></PaperNameGenerator>
    </div>
  </>
    ;
}

export default App;
